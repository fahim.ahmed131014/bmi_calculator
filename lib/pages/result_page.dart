import 'package:bmi_calculator/models/bmi_calculator.dart';
import 'package:bmi_calculator/models/bottom_button.dart';
import 'package:bmi_calculator/models/resuable_container.dart';
import 'package:bmi_calculator/pages/input_page.dart';
import 'package:bmi_calculator/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ResultPage extends StatelessWidget {
  String? bmiValue;
  String? bmiStatus;
  String? bmiComment;

  ResultPage({@required this.bmiValue,@required this.bmiStatus,@required this.bmiComment });


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appName),
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return InputPage();
              },
            )
            );
          },
          child: Icon(FontAwesomeIcons.backward),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(top:20),
                child: Text("Your Result", style: kNumberTextStyle,
                textAlign: TextAlign.center,),
              ),
            ),
            Expanded(
              flex: 6,
              child: ReusableContainer(
               color: kInActiveColor,
                containerChild: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("$bmiStatus",style:kBmiStatusText
                    ),
                    SizedBox(
                      height:kSizedBoxHeight,
                    ),
                    Text("$bmiValue",style: TextStyle(
                      fontSize:70,
                      fontWeight: FontWeight.bold,
                    ),),
                    SizedBox(
                      height:kSizedBoxHeight,
                    ),
                    Text("$bmiComment",style: kLabelTextStyle,
                    textAlign: TextAlign.center),
                  ],
                ),
              ),
            ),
            BottomButton(
                buttonName: "Recalculate BMI".toUpperCase(),
                onPressed: (){
                  Navigator.pop(context);
                }
            ),


          ],
        ),
      ),
    );
  }
}
