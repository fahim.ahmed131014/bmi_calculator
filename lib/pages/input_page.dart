import 'package:bmi_calculator/main.dart';
import 'package:bmi_calculator/models/bmi_calculator.dart';
import 'package:bmi_calculator/models/bottom_button.dart';
import 'package:bmi_calculator/models/gender_column.dart';
import 'package:bmi_calculator/models/resuable_container.dart';
import 'package:bmi_calculator/models/reusable_circle.dart';
import 'package:bmi_calculator/pages/result_page.dart';
import 'package:bmi_calculator/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


enum Gender{
  male,
  female
}
Gender? selectedGender;
int heightValue= 150;
int weight = 74;
int age = 19;



class InputPage extends StatefulWidget {
  const InputPage({Key? key}) : super(key: key);

  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(appName),
          centerTitle: true,
          leading: const Icon(Icons.menu_sharp),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: Row(
                  children: [
                    Expanded(
                    child: ReusableContainer(
                      onPressed: () {
                        setState(() {
                          selectedGender = Gender.male;
                        });
                      },
                      color: selectedGender == Gender.male
                          ? kActiveColor
                          : kInActiveColor,
                      containerChild: GenderColumn(
                        icon: FontAwesomeIcons.mars,
                        label: "MALE",
                      ),
                    ),
                  ),
                    Expanded(
                      child: ReusableContainer(
                    onPressed: (){
                        setState(() {
                          selectedGender = Gender.female;
                        });
                      },
                      color: selectedGender == Gender.female
                          ? kActiveColor
                          : kInActiveColor,
                      containerChild: GenderColumn(
                          icon: FontAwesomeIcons.venus,
                          label: "FEMALE",
                        ),
                      ),
                  ),
                  ],
                ),
            ),
            Expanded(
              child:ReusableContainer(
                color: kInActiveColor,
                containerChild: Column(
                  mainAxisAlignment: kColumnAxisAlignment,
                  children: [
                    Text("height".toUpperCase(),style:kLabelTextStyle),
                    Row(
                      mainAxisAlignment: kColumnAxisAlignment,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: [
                        Text("$heightValue",style: kNumberTextStyle),
                        Text("cm",style: kLabelTextStyle,),
                      ],
                    ),
                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                         thumbShape: RoundSliderThumbShape(
                           enabledThumbRadius: 15,
                         ),
                        overlayShape: RoundSliderOverlayShape(
                          overlayRadius: 20,
                        ),
                        overlayColor: Color(0xFFffffff),
                        activeTrackColor: Color(0xFFa02876),
                        inactiveTrackColor: Color(0xFFbfc4e3),
                        thumbColor: Color(0xFFa02876),
                      ),
                      child: Slider(
                        value: heightValue.toDouble(),
                        min:100,
                        max: 200,

                        inactiveColor: Color(0xFFbfc4e3),
                        onChanged: (newValue){
                          setState(() {
                            heightValue = newValue.round();
                          });
                          },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
                child: Row(
                  children: [
                  Expanded(
                    child: ReusableContainer(
                      color: kInActiveColor,
                      containerChild: Column(
                        mainAxisAlignment: kColumnAxisAlignment,
                        children: [
                          Text(
                              "Weight".toUpperCase(),style:kLabelTextStyle),
                          const SizedBox(
                              height: kSizedBoxHeight,
                          ),
                          Text(
                              "$weight",style:kNumberTextStyle),
                          const SizedBox(
                            height: kSizedBoxHeight,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: ReusableCircle(
                                  buttonIcon: Icons.remove,
                                  onPressed: (){
                                    setState(() {
                                      if(weight>10 && weight<200)
                                        {
                                          weight--;
                                        }
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: ReusableCircle(
                                  buttonIcon: Icons.add,
                                  onPressed: () {
                                    setState(() {
                                      if (weight > 10 && weight < 200) {
                                        weight++;
                                      }
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),


                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ReusableContainer(
                      color: kInActiveColor,
                      containerChild: Column(
                        mainAxisAlignment: kColumnAxisAlignment,
                        children: [
                          Text(
                              "Age".toUpperCase(),style:kLabelTextStyle),
                          const SizedBox(
                            height: kSizedBoxHeight,
                          ),
                          Text(
                              "$age",style:kNumberTextStyle),
                          const SizedBox(
                            height: kSizedBoxHeight,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: ReusableCircle(
                                  buttonIcon: Icons.remove,
                                  onPressed: () {
                                    setState(() {
                                      if (age > 10 && age < 100) {
                                        age--;
                                      }
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: ReusableCircle(
                                  buttonIcon: Icons.add,
                                  onPressed: () {
                                    setState(() {
                                      if (age > 10 && age < 100) {
                                        age++;
                                      }
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),


                    ),
                  ),
                ],
                ),
            ),
            BottomButton(
              buttonName: "Calculate Bmi".toUpperCase(),
              onPressed: (){
                BMICalculation bmiCalculation = BMICalculation(heightValue,weight);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return ResultPage(bmiValue: bmiCalculation.bmiCalculate(),
                      bmiComment: bmiCalculation.commentBMI(),
                      bmiStatus: bmiCalculation.statusBMI(),);
                  }),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}






