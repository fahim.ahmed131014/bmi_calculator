import 'package:flutter/material.dart';


const appName = "BMI Calculator";
const kLabelTextStyle = TextStyle(color: Color(0xFFeceff7),fontSize: 16,fontWeight: FontWeight.bold);
const kIconSize = 80.0;
const kBottomContainerHeight = 80.0;
const kActiveColor = Color(0xFF2839A0);
const kInActiveColor = Color(0xFF26294d);
const kSizedBoxHeight = 10.0;
const kColumnAxisAlignment = MainAxisAlignment.center;
const kNumberTextStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 40,);
const kIconColor = Color(0xFFeceff7);
const kCircleRadius = 42.0;
const kCircleIconSize = 50.0;
const kCircleBackgroundColor = Color(0xFF3e56aa);
const kBmiStatusText = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
