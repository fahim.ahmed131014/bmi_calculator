import 'package:flutter/material.dart';
import 'pages/input_page.dart';

void main() {
  runApp(
    BmiCalculator(),
  );
}

class BmiCalculator extends StatelessWidget {
  const BmiCalculator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: InputPage(),
    );
  }
}



