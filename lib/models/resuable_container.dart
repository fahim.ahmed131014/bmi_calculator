import 'package:flutter/material.dart';

class ReusableContainer extends StatelessWidget {
  final Color? color;
  final Widget? containerChild;
  final Function()? onPressed;
  ReusableContainer({@required this.color, @required this.containerChild,  this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: containerChild,
      ),
    );
  }
}
