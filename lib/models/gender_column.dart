import 'package:bmi_calculator/utils/constants.dart';
import 'package:flutter/material.dart';

class GenderColumn extends StatelessWidget {
  final IconData? icon;
  final String? label;
  GenderColumn({@required this.icon, @required this.label});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon,
          size: kIconSize),
        const SizedBox(
          height: kSizedBoxHeight,
        ),
        Text(label!,style: kLabelTextStyle,
        ),
      ],
    );
  }
}
