import 'package:bmi_calculator/utils/constants.dart';
import 'package:flutter/material.dart';

class ReusableCircle extends StatelessWidget {
 final IconData? buttonIcon;
 final Function()? onPressed;
 ReusableCircle({@required this.buttonIcon, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: CircleAvatar(
                radius: kCircleRadius,
                backgroundColor:kCircleBackgroundColor,
                child: Icon(buttonIcon,
                    size: kCircleIconSize,
                    color:kIconColor),
      ),
    );
  }
}