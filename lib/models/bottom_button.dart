import 'package:bmi_calculator/utils/constants.dart';
import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  final String? buttonName;
  final Function()? onPressed;
  BottomButton({@required this.buttonName,@required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.all(10),
        height: kBottomContainerHeight,
        color: const Color(0xFFff0067),
        child: Center(
          child: Text(buttonName!.toUpperCase(),
            style: kLabelTextStyle,
          ),
        ),

      ),
    );
  }
}