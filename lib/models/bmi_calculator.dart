import 'dart:math';

class BMICalculation
{
  int? _height;
  int? _weight;
  double? _bmi;

  BMICalculation(this._height, this._weight);

  String bmiCalculate()
  {
    _bmi = _weight! / pow(_height!/100, 2);
    return _bmi!.toStringAsFixed(1);
  }

  String statusBMI()
  {
    if(_bmi!<16.0)
      {
        return "UnderWeight";
      }
    else if(_bmi!>=16.0 && _bmi!<=16.9)
    {
      return "UnderWeight And Moderate Thinness";
    }
    else if(_bmi!>=17.0 && _bmi!<=18.4)
    {
      return "UnderWeight And Mild Thinness";
    }
    else if(_bmi!>=18.5 && _bmi!<=24.9)
    {
      return "Normal";
    } else if(_bmi!>=25.0 && _bmi!<=29.9)
    {
      return "Overweight";
    }
    else if(_bmi!>=30.0 && _bmi!<=34.9)
    {
      return "Overweight (Early Stage)";
    }
    else if(_bmi!>=35.0 && _bmi!<=39.9)
    {
      return "Overweight (Medium Stage)";
    }
    else
    {
      return "Obese";
    }
  }

  String commentBMI()
  {
    if(_bmi!<16.0)
    {
      return "You should eat more, exercise and word hard. Take care of your health";
    }
    else if(_bmi!>=16.0 && _bmi!<=16.9)
    {
      return "You should eat more, exercise and word hard. Take care of your health";
    }
    else if(_bmi!>=17.0 && _bmi!<=18.4)
    {
      return "You should eat more, exercise and word hard. Take care of your health";
    }
    else if(_bmi!>=18.5 && _bmi!<=24.9)
    {
      return "You are perfectly normal.. Keep doing that and follow the routine";
    } else if(_bmi!>=25.0 && _bmi!<=29.9)
    {
      return "You are gaining fat. Take care of your health";
    }
    else if(_bmi!>=30.0 && _bmi!<=34.9)
    {
      return "You are fat but in an early stage. You should look out your body. Exercise daily";
    }
    else if(_bmi!>=35.0 && _bmi!<=39.9)
    {
      return "You are going fatter . You should diet and exercise. Otherwise you will be out of control";
    }
    else
    {
      return "You are out of control. Take doctor's advice and look after your body";
    }
  }




}